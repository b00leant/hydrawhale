FROM node:12

MAINTAINER b00leant

RUN apt-get -y update
RUN apt-get -y install git
RUN git clone https://github.com/ojack/hydra.git

WORKDIR /hydra

RUN yarn install


# Default command
CMD ["yarn", "serve"]

EXPOSE 8000
