# `hydrawhale`

![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)

### A dockerized instance of Olivia Jack's [hydra](https://hydra.ojack.xyz/)

## Getting Started
Just launch your docker instance with `docker run -p 127.0.0.1:80:8080/tcp b00leant/hydrawhale:latest`

## Useful links
- [DockerHub page 🐳](https://hub.docker.com/r/b00leant/hydrawhale/tags)
- [Hydra doc 📜](https://hydra.ojack.xyz/docs/)